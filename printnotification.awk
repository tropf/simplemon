FNR==NR {
    last++
    next
}

END {
    if (FNR == NR) {
        print "[!!] feed this script the log file TWICE"
        exit
    }
}

BEGIN {
    max_ok_ram_cmd = "awk '/^MemTotal/{print $2 * 0.9}' /proc/meminfo"
    max_ok_ram_cmd | getline max_ok_ram
    close(max_ok_ram_cmd)

    max_ok_cpu_busyfrac = 0.8

    max_ok_usercount = 100

    usercount_alert = 0
    ram_alert = 0
    cpu_busyfrac_alert = 0

    errors_resolved = 1
}

{
    delta_cputicks_total = $12 - last_cputicks_total
    delta_cputicks_busy = $13 - last_cputicks_busy
    cpu_busyfrac = (0 == delta_cputicks_total) ? 0 : (delta_cputicks_busy / delta_cputicks_total)

    # to invoke error the last four line have to be
    # no error - error - error - error
    # (this deduplicates error messages)

    if (FNR == (last - 3)) {
        # 4th last line
        if ($2 <= max_ok_usercount) {
            usercount_alert = 1
        }

        if ($9 <= max_ok_ram) {
            ram_alert = 1
        }

        if (cpu_busyfrac <= max_ok_cpu_busyfrac) {
            cpu_busyfrac_alert = 1
        }
    }

    if (FNR > (last - 3)) {
        # in 3 last lines
        if ($2 <= max_ok_usercount) {
            usercount_alert = 0
        }

        if ($9 <= max_ok_ram) {
            ram_alert = 0
        }

        if (cpu_busyfrac <= max_ok_cpu_busyfrac) {
            cpu_busyfrac_alert = 0
        }
    }

    # errors resolved -> 3x error state, then no more error state
    if (FNR > (last - 4) && FNR != last) {
        # 4th last to 2nd last line
        if ($2 <= max_ok_usercount && $9 <= max_ok_ram && cpu_busyfrac <= max_ok_cpu_busyfrac) {
            errors_resolved = 0
        }
    }

    if (FNR == last) {
        # last line -> no more errors
        if ($2 > max_ok_usercount || $9 > max_ok_ram || cpu_busyfrac > max_ok_cpu_busyfrac) {
            errors_resolved = 0
        }
    }

    last_cputicks_total = $12
    last_cputicks_busy = $13
}

END {
    # note: AWK needs comparison to 1, bools are not supported
    if (1 == usercount_alert) {
        printf "BBB Alert: High user count (%s > %s)!\n", $2, max_ok_usercount
    }

    if (1 == ram_alert) {
        printf "BBB Alert: High RAM usage (%d MiB > %d MiB)!\n", ($9 / 1024), (max_ok_ram / 1024)
    }

    if (1 == cpu_busyfrac_alert) {
        printf "BBB Alert: CPU is very busy (%s%% > %s%%)!\n", int(cpu_busyfrac * 100), int(max_ok_cpu_busyfrac * 100)
    }

    if (1 == errors_resolved) {
        printf "BBB Info: Load back to normal (%d users, %d MiB RAM, %s loadavg, %s%% cpu busy)\n", $2, ($9 / 1024), $8, int(cpu_busyfrac * 100)
    }
}
