# note: invoke with -v header=true to print header
BEGIN {
    if (header) {
        print "timestamp\tbbb_total\tbbb_cam\tbbb_mic\tbbb_listen\ttx_kbps\trx_kbps\tloadavg_1min\tused_ram_kb\tread_kbps\twrite_kbps\tcpu_busy_fraction"
    }
}
{
    delta_time = $1 - last_time
    delta_tx_bytes = $6 - last_tx_bytes
    delta_rx_bytes = $7 - last_rx_bytes
    delta_read_sectors = $10 - last_read_sectors
    delta_write_sectors = $11 - last_write_sectors
    delta_cputicks_total = $12 - last_cputicks_total
    delta_cputicks_busy = $13 - last_cputicks_busy

    # bytes -> bits (*8) -> kbits (/1000)
    tx_kbps = int((delta_tx_bytes / delta_time) * 8 / 1000)
    rx_kbps = int((delta_rx_bytes / delta_time) * 8 / 1000)
    # sectors (unix sectors, 512 bytes) -> bytes (*512) -> bits (*8) -> kbits (/1000)
    read_kbps = int((delta_read_sectors / delta_time) * 512 * 8 / 1000)
    write_kbps = int((delta_write_sectors / delta_time) * 512 * 8 / 1000)
    # busy percent
    cpu_busy_fraction = (0 == delta_cputicks_total) ? 0 : (delta_cputicks_busy / delta_cputicks_total)

    if (delta_time >= 0 && delta_tx_bytes >= 0 && delta_rx_bytes >= 0 && delta_read_sectors >= 0 && delta_write_sectors >= 0) {
        printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", $1, $2, $3, $4, $5, tx_kbps, rx_kbps, $8, $9, read_kbps, write_kbps, cpu_busy_fraction
    }

    last_time = $1
    last_tx_bytes = $6
    last_rx_bytes = $7
    last_read_sectors = $10
    last_write_sectors = $11
    last_cputicks_total = $12
    last_cputicks_busy = $13
}
