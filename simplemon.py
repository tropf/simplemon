import sys
import urllib.request
import hashlib
import xml.etree.ElementTree as ET
import time

if len(sys.argv) < 6:
    print('usage: {} FILE URL SECRET NETDEV HDDDEV'.format(sys.argv[0]))
    exit(1)

logfile_path = sys.argv[1]
api_url_base = sys.argv[2]
api_secret = sys.argv[3]
netdev = sys.argv[4]
hdddev = sys.argv[5]

def get_getmeetings_url(url_base, secret):
    api_endpoint = 'getMeetings'
    hashed = hashlib.sha1('{}{}'.format(api_endpoint, secret).encode()).hexdigest()
    return '{}api/{}?checksum={}'.format(url_base, api_endpoint, hashed)

def get_response(url):
    with urllib.request.urlopen(url) as data:
        return data.read().decode('UTF-8')

def extract_counts(text):
    # note: do NOT extract room count, as this would expose avg room occupancy
    tree = ET.fromstring(text)
    cnts = {
        'total': 0,
        'cam': 0,
        'mic': 0,
        'listen': 0,
    }

    for meeting in tree.find('meetings').findall('meeting'):
        cnts['total'] += int(meeting.find('participantCount').text)
        cnts['cam'] += int(meeting.find('videoCount').text)
        cnts['mic'] += int(meeting.find('voiceParticipantCount').text)
        cnts['listen'] += int(meeting.find('listenerCount').text)

    return cnts

def get_tx_bytes(dev):
    with open('/sys/class/net/{}/statistics/tx_bytes'.format(dev), 'r') as f:
        return int(f.read())

def get_rx_bytes(dev):
    with open('/sys/class/net/{}/statistics/rx_bytes'.format(dev), 'r') as f:
        return int(f.read())

def get_loadavg1min():
    with open('/proc/loadavg', 'r') as f:
        for line in f:
            return float(line.split()[0])

def get_used_ram_kb():
    d = {}
    with open('/proc/meminfo', 'r') as f:
        for line in f:
            words = line.split()
            if len(words) >= 2:
                d[words[0]] = int(words[1])

    return d['MemTotal:'] - d['MemFree:'] - d['Cached:'] - d['Buffers:'] - d['SReclaimable:'] 

# see https://www.kernel.org/doc/Documentation/block/stat.txt
def get_read_sectors(dev):
    with open('/sys/block/{}/stat'.format(dev), 'r') as f:
        words = f.read().split()
        return int(words[2])

def get_write_sectors(dev):
    with open('/sys/block/{}/stat'.format(dev), 'r') as f:
        words = f.read().split()
        return int(words[6])

def get_cputimes():
    with open('/proc/stat', 'r') as f:
        for line in f:
            words = line.split()
            if 'cpu' == words[0]:
                times = {}
                times['total'] = sum(map(int, words[1:]))
                def get_word(index):
                    return words[index]
                times['busy'] = sum(map(int, map(get_word, [1, 3, 6, 7, 8, 9])))
                return times
        raise RuntimeError('did not find cpu line in /proc/stat')



now_utc = int(time.time())

with open(logfile_path, 'a') as logfile:
    #timestamp bbb_total bbb_cam bbb_mic bbb_listen tx_bytes rx_bytes loadavg_1min used_ram_kb read_sectors write_sectors cpu_ticks_total cpu_ticks_busy

    bbb_users = extract_counts(get_response(get_getmeetings_url(api_url_base, api_secret)))
    cputimes = get_cputimes()
    logfile.write(
        '{} {} {} {} {} {} {} {} {} {} {} {} {}\n'.format(
            now_utc,
            bbb_users['total'],
            bbb_users['cam'],
            bbb_users['mic'],
            bbb_users['listen'],
            get_tx_bytes(netdev),
            get_rx_bytes(netdev),
            get_loadavg1min(),
            get_used_ram_kb(),
            get_read_sectors(hdddev),
            get_write_sectors(hdddev),
            cputimes['total'],
            cputimes['busy'],
        )
    )

